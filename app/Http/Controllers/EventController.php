<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Restaurant;
use App\Models\User;
use App\Services\EventService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use function MongoDB\BSON\toJSON;

class EventController extends Controller
{
    protected $service;

    public function __construct(EventService $service)
    {
        $this->service = $service;
    }

    public function isOwner(Event $event, User $user)
    {
        return $event->user_id == $user->id;
    }

    public function list(): view
    {
        return view('events/list')->with('user');
    }

    public function show($id)
    {
        try {
            $event = Event::with('restaurants.dishes.ingredients')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect('/home')->with('error', 'Nie ma takiego wydarzenia');
        }

        if (!$event->group->users->where('id', Auth::id())->first()) {
            return redirect()->route('user.groups')->with('error', 'Nie masz uprawnień');
        }

        return view('events/show')
            ->with('event', $event)
            ->with('isOwner', $this->isOwner($event, Auth::user()));
    }

}
