<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    public function dishes(Request $request)
    {
        try {
            $restaurant = Restaurant::findOrFail($request->id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('home');
        }

        return $restaurant->dishes->toJson();
    }
}
