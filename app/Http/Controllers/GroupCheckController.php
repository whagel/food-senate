<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupUser;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class GroupCheckController extends GroupController
{
    public function groupCheck($id, $user_id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        try {
            User::findOrFail($user_id);
        } catch (ModelNotFoundException $e) {

            return redirect()->route('public.group.list')->with('error', 'Nie ma takiego użytkownika.');
        }

        try {
            GroupUser::where('user_id', $user_id)->where("group_id", $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {

            return redirect()->route('public.group.list')->with('error', 'Użytkownik nie należy do tej grupy.');
        }

        if (!$group->isOwner(Auth::user()) && !Auth::user()->isAdmin()) {
            return redirect()->route('public.group.list')->with('error', 'Nie masz uprawnień');
        }
    }

}