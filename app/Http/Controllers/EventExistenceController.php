<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEvent;
use App\Models\Event;
use App\Models\Group;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Redirect;

class EventExistenceController extends EventController
{
    public function createForm($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Grupa nie istnieje');
        }

        if(!$group->isMember( Auth::user())) {
            return redirect()->route('public.group.list')->with('error', 'Brak uprawnień');
        }

        return view('events/create')->with('group', $group);
    }

    public function create(StoreEvent $request, $id)
    {
        $this->service->create($request->all(), $id, Auth::user());
        return redirect()->route('group', $id)->with('success', 'Pomyślnie utworzono wydarzenie.');
    }

    public function editForm($id)
    {
        try {
            $event = Event::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiego wydarzenia');
        }

        if (!$this->isOwner($event, Auth::user())) {
            return redirect()->route('group', $event->group_id)->with('error', 'Brak uprawanień');
        }

        return view('events/edit')->with('event', $event);
    }

    public function edit(StoreEvent $request, $id)
    {
        try {
            $event = Event::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Wydarzenie nie istnieje.');
        }

        if (!$this->isOwner($event, Auth::user())) {
            return redirect()->route('$group', $event->group_id)->with('error', 'Brak uprawnień');
        }

        $this->service->edit($request->all(), $event);

        return redirect()->route('group', $event->group_id)->with('success', 'Pomyślnie edytowano wydarzenie');
    }

    public function removeForm($id)
    {
        try {
            $event = Event::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return Redirect::back()->with('error', 'Nie ma takiego wydarzenia.');
        }

        if (!$this->isOwner($event, Auth::user())) {
            return redirect()->route('group', $event->group->id)->with('error', 'Brak uprawnień');
        }

        return view('events.remove')->with('event', $event);
    }

    public function remove($id)
    {
        try {
            $event = Event::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return Redirect::back()->with('error', 'Nie ma takiego wydarzenia');
        }

        if ($this->isOwner($event, Auth::user())) {
            $this->service->remove($id);
        } else {
            return redirect()->route('group', $event->group->id)->with('error', 'Nie masz uprawnień');
        }

        return redirect()->route('group', $event->group->id)->with('info', 'Pomyślnie usunięto wydarzenie');
    }
}
