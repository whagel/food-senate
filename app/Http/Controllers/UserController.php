<?php

namespace App\Http\Controllers;

use App\Models\GroupType;
use App\Models\User;
use App\Services\UserService;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
        $this->middleware('auth', ['except' => ['activate']]);
    }

    public function getProfileView(Request $request)
    {
        if ($request->id) {
            try {
                $user = User::findOrFail($request->id);
            } catch (ModelNotFoundException $e) {
                return redirect()->route('home')->with('error', 'Nie ma takiego użytkownika');
            }
        } else {
            $user = Auth::user();
        }

        if ($user->isAdmin()) {
            return view('profile')->with('user', $user)->with('groups', $user->groups);
        } else {
            return view('profile')->with('user', $user)->with('groups', $user->groups->where('group_type_id', GroupType::PUBLIC));
        }
    }

    public function activate($id, $token)
    {
        try{
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect('/home')->with('error', 'Użytkownik nie istnieje');
        }

        $this->service->activate($token, $user);

        return view('/home')->with('success', 'aktytwowano konto');
    }
}
