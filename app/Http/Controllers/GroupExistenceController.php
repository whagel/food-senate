<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGroup;
use App\Models\Group;
use App\Models\GroupType;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class GroupExistenceController extends GroupController
{
    public function createFrom(): view
    {
        return view('groups/create')
            ->with('private', GroupType::PRIVATE)
            ->with('public', GroupType::PUBLIC);
    }

    public function create(StoreGroup $request): RedirectResponse
    {
        $this->service->addGroup($request->all(), Auth::user());
        return redirect()->route('user.groups');
    }

    public function edit(StoreGroup $request, $id): RedirectResponse
    {
        $this->service->editGroup(Group::find($id), $request->all());

        return redirect()->route('public.group.list');
    }

    public function editForm($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        if(!$group->isModerator(Auth::user()) && !$group->isOwner(Auth::user()) && !Auth::user()->isAdmin()) {
            return redirect()->route('public.group.list')->with('error', 'Nie masz uprawnień');
        }

        return view('groups/edit')->with('group', $group);
    }

    public function removeCheck($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        if(!$group->isOwner(Auth::user()) && !Auth::user()->isAdmin()) {
            return redirect()->route('public.group.list')->with('error', 'Nie masz uprawnień');
        }

        return view('groups/remove')->with('group', $group);
    }

    public function remove($id): RedirectResponse
    {
        $this->service->removeGroup($id);
        return redirect()->route('public.group.list');
    }

}