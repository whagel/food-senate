<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AdminPermissionController extends AdminController
{
    public function userPromoteCheck($id): view
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        return view('user.promote')->with('user', $user);
    }

    function userPromote($id): RedirectResponse
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        $this->service->promoteUser($id);

        return redirect('/admin/panel')->with('success', 'Nadano rangę administratora użytkownikowi ' . $user->name . '.');
    }

    public function userDemoteCheck($id): view
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        return view('user.demote')->with('user', $user);
    }

    function userDemote($id): RedirectResponse
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        $this->service->setDefaultRank($id);

        return redirect('/admin/panel')->with('success', 'Zabrano rangę administratora użytkownikowi ' . $user->name . '.');
    }
}