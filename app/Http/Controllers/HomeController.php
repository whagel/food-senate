<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class HomeController extends Controller
{
    public function getIndexView(): view
    {
        return view('root');
    }
}
