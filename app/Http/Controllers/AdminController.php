<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AdminService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AdminController extends Controller
{
    public $service;

    public function __construct(AdminService $service)
    {
        $this->service = $service;
    }

    public function userBanCheck($id): view
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        return view('user.ban')->with('user', $user);
    }

    function userBan($id): RedirectResponse
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        $this->service->banUser($id);

        return redirect('/admin/panel')->with('success', 'Zbanowano użytkownika ' . $user->name . '.');
    }

    public function userUnbanCheck($id): view
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        return view('user.unban')->with('user', $user);
    }

    function userUnban($id): RedirectResponse
    {
        try {
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('/admin/panel')->with('error', 'Nie ma takiego użytkownika.');
        }

        $this->service->setDefaultRank($id);

        return redirect('/admin/panel')->with('success', 'Odbanowano użytkownika ' . $user->name . '.');
    }

}