<?php

namespace App\Http\Controllers;


use App\Models\Restaurant;
use App\Models\Event;
use App\Models\Vote;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class VoteController extends Controller
{
    public function storeVote($event_id, $restaurant_id)
    {
        try {
            $event = Event::findOrFail($event_id);
        } catch (ModelNotFoundException $e) {
            return Redirect::back()->with('error', 'Nie ma takiego wydarzenia.');
        }

        if ($restaurant_id == 0) {

        } else {
            try {
                $restaurant = Restaurant::findOrFail($restaurant_id);
            } catch (ModelNotFoundException $e) {
                return Redirect::back()->with('error', 'Nie ma takiej restauracji.');
            }
        }

        if ($event->group->isMember(Auth::user())) {
            Vote::updateOrCreate([
                'event_id' => $event_id,
                'user_id' => Auth::user()->id,
            ], [
                'restaurant_id' => $restaurant_id,
            ]);
            return Redirect::route('event.show', $event_id);
        } else {
            return Redirect::back()->with('error', 'Nie masz uprawnień.');
        }
    }

    public function countVotes($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
        } catch (ModelNotFoundException $e) {
            return Redirect::back()->with('error', 'Nie ma takiego wydarzenia.');
        }

        foreach($event->restaurants as $restaurant){
            $restaurant->votes_count = $restaurant->votes->count();
            $restaurant->save();
        }
        $restaurant = $event->restaurants->sortByDesc('votes_count')->first();
        $collection = collect($event->restaurants->where('votes_count', $restaurant->votes_count)->all());
        $winner = $collection->shuffle()->first();

        $event->is_finished = true;
        $event->save();

        return view('events/finish')
            ->with('event', $event)
            ->with('winner', $winner);
    }
}