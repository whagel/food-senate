<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\InviteToken;
use App\Services\GroupInviteService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GroupInviteController extends GroupController
{
    protected $service;

    public function __construct(GroupInviteService $service)
    {
        $this->service = $service;
    }

    public function getInviteView($id): view
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        return view('groups/invite')->with('group', $group);
    }

    public function storeToken($id, Request $request)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }


        $invitetoken = InviteToken::firstOrCreate(['group_id' => $group->id]);
        $token = $this->service->generateToken();
        $invitetoken->token = $token;
        if ($request->expiration != null) {
            $date = Carbon::parse($request->expiration);
        } else {
            $date = Carbon::now();
        }
        $invitetoken->expiration_date = $date;
        $invitetoken->is_permanent = $request->permanent == "true";
        $invitetoken->save();

        return $token;

    }
}