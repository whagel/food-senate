<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Role;
use App\Models\User;
use Illuminate\View\View;

class AdminViewsController extends Controller
{
    public function getAdminPanelView(): view
    {
        return view('admin.panel');
    }

    public function getGroupsListView(): view
    {
        $groups = Group::paginate(10);
        return view('admin.groups')->with('groups', $groups);
    }

    public function getUsersListView(): view
    {
        $users = User::where('role_id', Role::USER)->paginate(10);
        return view('admin.users')->with('users', $users);
    }

    public function getBannedUsersListView(): view
    {
        $users = User::where('role_id', Role::BANNED)->orderBy('banned_at', 'desc')->paginate(10);
        return view('admin.banned')->with('users', $users);
    }

    public function getAdminsListView(): view
    {
        $users = User::where('role_id', Role::ADMIN)->paginate(10);
        return view('admin.admins')->with('users', $users);
    }

}