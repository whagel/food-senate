<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupType;
use App\Services\GroupService;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\View\View;

class GroupController extends Controller
{
    protected $service;

    public function __construct(GroupService $service)
    {
        $this->service = $service;
    }

    public function publicList(): view
    {
        $groups = Group::where('group_type_id', GroupType::PUBLIC)->paginate(12);
        return view('groups/publicList')->with('groups', $groups);
    }

    public function groups(): view
    {
        return view('groups/groups')->with('groups', Auth::user()->groups);
    }

    public function show($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie można wyświetlić grupy.');
        }

        $member = Auth::user();
        $member = $member->groups->find($id);

        if($group->group_type_id == GroupType::PRIVATE && !$member && !Auth::user()->isAdmin())
        {
            return redirect()->route('public.group.list')->with('error', 'Nie masz uprawnień');
        }
        $date = $group->inviteToken->expiration_date;
        $date = Carbon::parse($date)->format('d.m.Y H:i');
        return view('groups/show')
            ->with('group', $group)
            ->with('member', $member)
            ->with('date', $date);
    }

    public function subscribe($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        if (!$group->isMember(Auth::user())) {
            return redirect()->route('public.group.list')->with('error', 'Nie jesteś członkiem tej grupy');
        }

        $this->service->subscribeGroup($id);

        return redirect()->route('group', $id)->with('success', 'Będziesz otrzymywać wiadomości o wydarzeniach.');
    }

    public function unsubscribe($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {

            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        if (!$group->isMember(Auth::user())) {
            return redirect()->route('public.group.list')->with('error', 'Nie jesteś członkiem tej grupy');
        }

        $this->service->unsubscribeGroup($id);

        return redirect()->route('group', $id)->with('info', 'Nie będziesz już otrzymywać wiadomości o wydarzeniach.');
    }

}