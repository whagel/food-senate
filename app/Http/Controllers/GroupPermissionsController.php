<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupUser;
use App\Models\User;

class GroupPermissionsController extends GroupCheckController
{
    public function groupUserTransferCheck($id, $user_id)
    {
        $user = User::findOrFail($user_id);
        if ($user->isBanned())
            return redirect()->route('public.group.list')->with('error', 'Użytkownik jest zablokowany.');

        $check = $this->groupCheck($id, $user_id) ?: null;
        if($check) {
            return $check;
        }

        $group = Group::findOrFail($id);
        $group_user = GroupUser::where('user_id', '=', $user_id)->where('group_id', $id);

        return view('groups/transfer')->with('group', $group)->with('group_user', $group_user);
    }

    public function groupUserTransfer($group_id, $user_id)
    {
        $user = User::findOrFail($user_id);
        $this->service->transferGroupUser($group_id, $user_id);

        return redirect()->route('group', $group_id)->with('success', 'Przekazano rangę właściciela użytkownikowi '. $user->name . '.');
    }

    public function groupUserPromoteCheck($id, $user_id)
    {
        $user = User::findOrFail($user_id);
        if ($user->isBanned())
            return redirect()->route('public.group.list')->with('error', 'Użytkownik jest zablokowany.');

        $check = $this->groupCheck($id, $user_id) ?: null;
        if($check) {
            return $check;
        }

        $group = Group::findOrFail($id);
        $group_user = GroupUser::where('user_id', '=', $user_id)->where('group_id', $id);

        return view('groups/promote')->with('group', $group)->with('group_user', $group_user);
    }

    public function groupUserPromote($group_id, $user_id)
    {
        $user = User::findOrFail($user_id);
        $this->service->promoteGroupUser($group_id, $user_id);

        return redirect()->route('group', $group_id)->with('success', 'Nadano rangę moderatora użytkownikowi '. $user->name . '.');
    }

    public function groupUserDemoteCheck($id, $user_id)
    {
        $user = User::findOrFail($user_id);
        if ($user->isBanned())
            return redirect()->route('public.group.list')->with('error', 'Użytkownik jest zablokowany.');

        $check = $this->groupCheck($id, $user_id) ?: null;
        if($check) {
            return $check;
        }

        $group = Group::findOrFail($id);
        $group_user = GroupUser::where('user_id', '=', $user_id)->where('group_id', $id);

        return view('groups/demote')->with('group', $group)->with('group_user', $group_user);
    }

    public function groupUserDemote($group_id, $user_id)
    {
        $user = User::findOrFail($user_id);
        $this->service->demoteGroupUser($group_id, $user_id);

        return redirect()->route('group', $group_id)->with('info', 'Odebrano rangę moderatora użytkownikowi '. $user->name . '.');
    }

}