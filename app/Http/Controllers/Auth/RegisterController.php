<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserActivation;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $token = str_random(20);

        $ac = new UserActivation();
        $ac->token = $token;
        $ac->user_id = $user->id;
        $ac->save();

        $data = ['id' => $user->id, 'token' => $token];

        Mail::send('emails.activate', $data, function ($message) use ($user) {
            $message->to($user->email);
            $message->subject('Food-Senate - aktywacja konta');
        });

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('success', 'Wysłano link aktywacyjny.');
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed|max:30',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     */

    protected function create(array $data)
    {
        return User::create([
            'name' => explode("@", $data['email'])[0],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'avatar' => '',
        ]);
    }
}