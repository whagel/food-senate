<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupType;
use App\Models\GroupUser;
use App\Models\InviteToken;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;

class GroupRoamController extends GroupCheckController
{
    public function joinCheck($id)
    {
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }
        if ($group->group_type_id == GroupType::PRIVATE) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        $member = Auth::user();
        $member = $member->groups->find($id);
        if ($member) {
            return redirect()->route('group',$id)->with('info', 'Należysz już do tej grupy.');
        }

        return view('groups/join')->with(['group' => $group, 'member' => $member]);
    }

    public function join($id): RedirectResponse
    {
        $this->service->joinGroup($id);

        return redirect()->route('group', ['id' => $id]);
    }

    public function joinByToken($token): RedirectResponse
    {
        $token = InviteToken::where('token', '=', $token)->first();
        if ($token == null) {
            return redirect()->route('public.group.list')->with('error', 'Podany link jest niepoprawny.');
        }
        if ($token->hasExpired()) {
            return redirect()->route('public.group.list')->with('error', 'Podany link wygasł.');
        }
        $group = Group::find($token->group_id);
        if ($group->isMember(Auth::user())) {
            return redirect()->route('group', $token->group_id)->with('info', 'Należysz już do tej grupy.');
        }

        $this->service->joinGroup($token->group_id);

        return redirect()->route('group', $token->group_id);
    }

    public function leaveCheck($id)
    {
        try {
            $group = Group::find($id);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('public.group.list')->with('error', 'Nie ma takiej grupy.');
        }

        $member = Auth::user();
        $member = $member->groups->find($id);

        if (!$member) {
            return redirect()->route('public.group.list')->with('error', 'Nie należysz do tej grupy.');
        }

        return view('groups/leave')->with('group', $group);
    }

    public function leave($id): RedirectResponse
    {
        $this->service->leaveGroup($id);

        return redirect()->route('public.group.list');
    }

    public function groupUserKickCheck($id, $user_id)
    {
        $check = $this->groupCheck($id, $user_id) ?: null;
        if($check) {
            return $check;
        }

        $group = Group::findOrFail($id);
        $group_user = GroupUser::where('user_id', '=', $user_id)->where('group_id', $id);

        if (!$group->isOwner(Auth::user()) && !Auth::user()->isAdmin()) {
            return redirect()->route('public.group.list')->with('error', 'Nie masz uprawnień');
        }

        return view('groups/kick')->with('group', $group)->with('group_user', $group_user);
    }

    public function groupUserKick($group_id, $user_id)
    {
        $this->service->kickGroupUser( $group_id, $user_id);

        return redirect('/groups')->with('info', 'Usunięto użytkownika z grupy.');
    }

}