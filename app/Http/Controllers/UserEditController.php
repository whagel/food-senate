<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAvatar;
use App\Http\Requests\StorePassword;
use App\Http\Requests\StoreUser;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class UserEditController extends UserController
{
    public function getProfileEditForm(): view
    {
        return view('user.profile')->with('user', Auth::user());
    }

    public function getPasswordEditForm(): view
    {
        return view('user.password');
    }

    public function editProfile(StoreUser $request)
    {
        $this->service->updateProfile($request);
        return redirect('/editProfile')->with('success', 'Edytowano profil.');
    }

    public function updatePasswordForm(StorePassword $request): RedirectResponse
    {
        $this->service->updatePassword($request);
        return Redirect('/profile');
    }

    public function updateAvatarForm(StoreAvatar $request): RedirectResponse
    {
        $this->service->updateAvatar($request->file('avatar'));
        return Redirect('/profile');
    }

}
