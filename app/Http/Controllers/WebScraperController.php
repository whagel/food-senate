<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use App\Models\Event;
use App\Models\Group;
use App\Models\Ingredient;
use App\Models\Restaurant;
use App\Models\Type;
use App\Services\DataService;
use Goutte\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Request;
use Mockery\Exception;
use PhpParser\Node\Expr\Cast\Int_;
use Symfony\Component\Process\Exception\InvalidArgumentException;

class WebScraperController extends Controller
{
    protected $event;
    protected $restaurants;
    protected $restaurant;
    protected $dishes;

    public function getRestaurants($event)
    {
        $this->event = $event;

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.pyszne.pl/' . $event->location);

        $crawler->filter('div.restaurant')->each(function ($node) {
            $name = $node->filter('a.restaurantname');

            if ($node->filter('div.open')->text() != 'Zamknięte' && $name->text() != "{{RestaurantName}}") {
                $this->restaurants[] = new Restaurant([
                    'name' => trim($name->text()),
                    'event_id' => $this->event->id,
                ]);
            }
        });

        $this->event->restaurants()->saveMany($this->restaurants);
    }

    public function getDishes()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.pyszne.pl/' . $this->event->location);

        $crawler->filter('div.restaurant')->each(function ($node) {
            $href = $node->filter('a')->attr("href");
            $open = $node->filter('div.open');
            $restaurant = Restaurant::where('name', trim($node->filter('a.restaurantname')->text()))->where('event_id', $this->event->id)->first();

            $client = new Client();
            $crawler = $client->request('GET', 'https://www.pyszne.pl/' . $href);

            if ($open->text() != 'Zamknięte' && $node->filter('a.restaurantname')->text() != "{{RestaurantName}}") {
                $crawler->filter('div.meal')->each(function ($node) {
                    $menu = $node->filter('span.meal-name');
                    $price = floatval(str_replace(',', '.', $node->filter('div.meal-add-btn-wrapper')->text()));

                    $this->dishes[] = new Dish([
                        'name' => $menu->text(),
                        'price' => $price,
                    ]);
                });
                if($open->text() != 'Zamknięte') {
                    $restaurant->dishes()->saveMany($this->dishes);
                }
                $this->dishes = [];
            }
        });
    }

    public function getRestaurantTypes()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.pyszne.pl/' . $this->event->location);

        $crawler->filter('div.restaurant')->each(function ($node) {
            $restaurant = Restaurant::where('name', trim($node->filter('a.restaurantname')->text()))->where('event_id', $this->event->id)->first();

            $kitchens = explode(',', $node->filter('div.kitchens')->text());
            $types = [];

            foreach ($kitchens as $kitchen) {
                $types[] = new Type([
                    'name' => $kitchen
                ]);
            }

            if ($node->filter('div.open')->text() != 'Zamknięte' && $node->filter('a.restaurantname')->text() != '{{RestaurantName}}' ) {
                $restaurant->types()->saveMany($types);
            }
        });
    }

    public function getIngredients()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.pyszne.pl/' . $this->event->location);

        $crawler->filter('div.restaurant')->each(function ($node) {
            $href = $node->filter('a')->attr("href");
            $open = $node->filter('div.open');

            $restaurant = $node->filter('a.restaurantname');
            $this->restaurant = Restaurant::where('name', trim($restaurant->text()))->where('event_id', $this->event->id)->first();

            $client = new Client();
            $crawler = $client->request('GET', 'https://www.pyszne.pl/' . $href);

            if ($open->text() != 'Zamknięte' && $node->filter('a.restaurantname')->text() != '{{RestaurantName}}' ) {
                $crawler->filter('div.meal')->each(function ($node) {
                    $menu = $node->filter('span.meal-name');

                    $meal_description_choose = '';
                    $meal_description_additional = '';

                    try {
                        $meal_description_additional = $node->filter('div.meal-description-additional-info')->text();
                    } catch (\InvalidArgumentException $e) {}

                    try {
                        $meal_description_choose = $node->filter('div.meal-description-choose-from')->text();
                    } catch (\InvalidArgumentException $e) {}

                    $descriptions = explode(',', $meal_description_additional.' '.$meal_description_choose);


                    $dish = Dish::where('name', $menu->text())
                        ->where('restaurant_id', $this->restaurant->id)
                        ->first();

                    $this->dish = $dish;

                    $ingredients = [];

                    foreach ($descriptions as $description) {
                          if($description != ' ') {
                              $meal = new Ingredient();

                              $meal->name = $description;
                              $meal->dish_id = $dish->id;
                              $meal->type_id = 1;

                              $ingredients[] = $meal;
                          }
                    }

                    $dish->ingredients()->saveMany($ingredients);
                    });
            }
        });
    }
}