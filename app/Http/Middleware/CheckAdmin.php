<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckAdmin
{
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->isAdmin()) {
            return redirect()->route('home')->with('error', 'Nie masz uprawnień!');
        }

        return $next($request);
    }
}
