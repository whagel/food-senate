<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckNotBanned
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->isBanned()) {
            return redirect()->route('home')->with('error', 'Zostałeś zbanowany!');
        }

        return $next($request);
    }
}
