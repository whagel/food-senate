<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAvatar extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'avatar' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'avatar.required' => 'Musisz wybrać avatar',
            'avatar.mimes' => 'Nieprawidłowy format pliku',
            'avatar.size' => 'Obrazek nie może być większy niż 2MB',
        ];
    }
}