<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDescription extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'description' => 'string|max:255',
        ];
    }

    public function messages()
    {
        return [
            'description.max' => 'Opis jest za długi!',
        ];
    }
}