<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePassword extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password' => 'required|string|min:6|confirmed|max:30',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Hasło jest wymagane',
            'password.min' => 'Hasło musi mieć co najmniej 6 znaków',
            'password.max' => 'Hasło musi mieć co najwyżej 30 znaków',
            'password.confirmed' => 'Hasła nie zgadzają się',

        ];
    }
}