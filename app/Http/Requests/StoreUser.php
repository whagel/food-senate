<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:25',
            'description' => 'max:255',
        ];
    }

    public function messages()
    {
        return [
            'description.max' => 'Opis jest za długi!',
            'name.required' => 'Należy podać nazwę',
            'name.min' => 'Nazwa musi mieć co najmniej 3 znaki',
            'name.max' => 'Nazwa może mieć co najwyżej 25 znaków',
        ];
    }
}