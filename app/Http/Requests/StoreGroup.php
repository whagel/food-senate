<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGroup extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:25',
            'location' => 'required|string|min:3|max:25',
        ];
    }

    public function messages()
    {
        return [
            'location.required' => 'Należy podać lokację.',
            'name.required' => 'Należy podać nazwę.',
            'name.min' => 'Nazwa musi mieć co najmniej 3 znaki',
            'name.max' => 'Nazwa może mieć co najwyżej 25 znaków',
            'location.min' => 'Lokacja musi mieć co najmniej 3 znaki',
            'location.max' => 'Lokacja może mieć co najwyżej 25 znaków',
        ];
    }

}