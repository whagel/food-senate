<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEvent extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:25',
            'description' => 'max:255',
            'location' => 'max: 30'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Należy podać nazwę.',
            'name.min' => 'Nazwa musi mieć co najmniej 3 znaki',
            'name.max' => 'Nazwa może mieć co najwyżej 25 znaków',
            'description.max' => 'Opis jest zbyt długi',
            'location.max' => 'Lokalizacja może mieć co najwyżej 30 znaków'
        ];
    }

}