<?php

namespace App\Observers;

use App\Models\Group;
use App\Models\GroupRole;
use App\Models\InviteToken;
use App\Services\GroupInviteService;
use Illuminate\Support\Facades\Auth;

class GroupObserver
{
    protected $inviteservice;
    public function __construct(GroupInviteService $inviteservice)
    {
        $this->inviteservice = $inviteservice;
    }

    public function created(Group $group)
    {
        $group->users()->attach(Auth::user(), ['group_role_id' => GroupRole::OWNER]);

        InviteToken::create([
            'group_id' => $group->id,
            'token' => $this->inviteservice->generateToken(),
        ]);
    }
}