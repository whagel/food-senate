<?php

namespace App\Observers;

use App\Http\Controllers\WebScraperController;
use App\Models\Event;
use Mail;

class EventObserver
{
    protected $scrapper;

    public function __construct(WebScraperController $scrapper)
    {
        $this->scrapper = $scrapper;
    }

    public function created(Event $event)
    {
        $this->scrapper->getRestaurants($event);
        $this->scrapper->getRestaurantTypes();
        $this->scrapper->getDishes();
        $this->scrapper->getIngredients();

        foreach ($event->group->groupUsers as $group_user) {
            if ($group_user->subscription) {

                Mail::send('emails.event', $data = ['event' => $event, 'user' => $group_user->user, 'group' => $event->group], function ($message) use ($group_user) {
                    $message->to($group_user->user->email);
                    $message->subject('Food-Senate - Nadchodzące wydarzenie');
                });
            }
        }
    }
}