<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\HasOne;

class Vote extends Model
{
    protected $fillable = [
        'event_id', 'user_id', 'restaurant_id'
    ];

    public $timestamps = false;

    public function event(): HasOne
    {
        return $this->hasOne(Event::class);
    }

    public function restaurant(): HasOne
    {
        return $this->hasOne(Restaurant::class);
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }
}
