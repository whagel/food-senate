<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class GroupRole extends Model
{
    const OWNER = 1;
    const MODERATOR = 2;
    const MEMBER = 3;

    public $timestamps = false;

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
