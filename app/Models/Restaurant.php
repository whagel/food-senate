<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class Restaurant extends Model
{
    protected $fillable = [
        'name', 'event_id'
    ];

    public function dishes(): HasMany
    {
        return $this->hasMany(Dish::class);
    }

    public function types(): BelongsToMany
    {
        return $this->belongsToMany(Type::class, 'restaurant_types');
    }

    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public function votes(): HasMany
    {
        return $this->hasMany(Vote::class);
    }

    public function isVoted($event_id){
        return Vote::where('event_id', $event_id)->where('user_id', Auth::user()->id)->where('restaurant_id', $this->id)->get()->isNotEmpty();

    }
}
