<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class IngredientType extends Model
{
    public $timestamps = false;

    public function ingredients(): BelongsToMany
    {
        return $this->belongsToMany(Ingredient::class);
    }
}
