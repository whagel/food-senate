<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class InviteToken extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'group_id', 'token', 'expiration_date', 'is_permanent'
    ];

    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    public function isPermanent()
    {
        return $this->is_permanent;
    }

    public function hasExpired()
    {
        if ($this->isPermanent()){
            return false;
        }
        return $this->expiration_date < Carbon::now();
    }

}
