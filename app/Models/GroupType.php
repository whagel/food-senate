<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class GroupType extends Model
{
    const PUBLIC = 1;
    const PRIVATE = 2;

    public static $types = [self::PRIVATE, self::PUBLIC];

    public $timestamps = false;

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class);
    }
}
