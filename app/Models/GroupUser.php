<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GroupUser extends Model
{
    protected $table = "group_users";

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
