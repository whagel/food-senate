<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Group extends Model
{
    protected $fillable = [
        'name', 'location', 'group_type_id'
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'group_users')->withPivot('group_role_id');
    }

    public function groupUsers(): HasMany
    {
        return $this->hasMany(GroupUser::class);
    }

    public function groupType(): HasOne
    {
        return $this->hasOne(GroupType::class, 'group_type_id');
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    public function inviteToken(): HasOne
    {
        return $this->hasOne(InviteToken::class);
    }

    public function isPublic(): bool
    {
        return $this->group_type_id == GroupType::PUBLIC;
    }

    public function isMember(User $user): bool
    {
        return $this->users()->find($user->id) != null;
    }

    public function isSubscribed($id, User $user): bool
    {
        if ($this->users()->find($user->id)->groupUsers->where('group_id', $id)->first()->subscription)
            return true;
        return false;
    }

    public function isModerator(User $user): bool
    {
        $moderators = $this->users()->wherePivot('group_role_id', '=', GroupRole::MODERATOR)->get();
        foreach ($moderators as $moderator) {
            if ($user->id == $moderator->id) return true;
        }
        return false;
    }

    public function isOwner(User $user): bool
    {
        $owners = $this->users()->wherePivot('group_role_id', '=', GroupRole::OWNER)->get();
        foreach ($owners as $owner) {
            if ($user->id == $owner->id) return true;
        }
        return false;
    }

    public function hasOwner()
    {
        $owners = $this->users()->wherePivot('group_role_id', '=', GroupRole::OWNER)->get()->filter(function ($user) {
            return $user->role_id != Role::BANNED;
        });
        return $owners->isNotEmpty();
    }
}
