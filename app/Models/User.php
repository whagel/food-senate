<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'description'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    public function groupUsers(): HasMany
    {
        return $this->hasMany(GroupUser::class);
    }

    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_users');
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    public function votes(): BelongsToMany
    {
        return $this->belongsToMany(Vote::class);
    }

    public function getAvatarPath()
    {
        if ($this->isBanned()) {
            return asset('blocked.jpg');
        }
        if ($this->avatar != '') {
            return asset('storage/avatars/' . $this->avatar);
        }

        return asset('default_av.jpg');
    }

    public function isAdmin(): Bool
    {
        return $this->role_id == Role::ADMIN || $this->role_id == Role::SUPERADMIN;
    }

    public function isSuperAdmin(): Bool
    {
        return $this->role_id == Role::SUPERADMIN;
    }

    public function isBanned(): Bool
    {
        return $this->role_id == Role::BANNED;
    }

    public function activationKey():HasOne
    {
        return $this->hasOne(UserActivation::class);
    }
}
