<?php

namespace App\Providers;

use App\Models\Group;
use App\Models\Event;
use App\Observers\GroupObserver;
use App\Observers\EventObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Group::observe(GroupObserver::class);
        Event::observe(EventObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
