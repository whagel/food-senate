<?php

namespace App\Services;


use Illuminate\Support\Facades\DB;

class GroupInviteService
{
    public function generateToken(): string
    {
        do {
            $token = str_random(16);
        } while (DB::table('invite_tokens')->where('token', '=', $token)->first());
        return $token;
    }

}