<?php

namespace App\Services;

use App\Models\Group;
use App\Models\GroupRole;
use App\Models\GroupUser;
use App\Models\User;
use Auth;
use Illuminate\Http\RedirectResponse;

class GroupService
{
    protected function setRole(int $group_id, int $user_id, $role): void
    {
        $group_user = GroupUser::where('user_id', $user_id)->where('group_id', $group_id)->first();
        $group_user->group_role_id = $role;
        $group_user->save();
    }

    public function addGroup($request_data): RedirectResponse
    {
        $group = new Group();
        $group->fill($request_data);
        $group->save();

        return redirect()->route('public.group.list')->with('success', 'Pomyślnie utworzono grupę.');
    }

    public function removeGroup(int $id): RedirectResponse
    {
        $group = Group::find($id);
        $name = $group->name;
        $group->users()->detach();
        $group->inviteToken()->delete();
        $group->delete();

        return redirect()->route('public.group.list')->with('success', 'Usunięto grupę '.$name.".");
    }

    public function editGroup(Group $group, $request_data): RedirectResponse
    {
        $group->fill($request_data);
        $group->save();

        return redirect()->route('public.group.list')->with('success', 'Pomyślnie edytowano grupę.');
    }

    public function joinGroup(int $id): RedirectResponse
    {
        $group = Group::find($id);
        $group->users()->attach(Auth::id(), ['group_role_id' => GroupRole::MEMBER]);

        return redirect()->route('public.group.list')->with('success', 'Dołączyłeś do grupy ' . $group->name . '.');
    }

    public function leaveGroup(int $id): RedirectResponse
    {
        $user = User::find(Auth::id());
        $user->groups()->detach($id);
        $group = Group::find($id);
        return redirect()->route('public.group.list')->with('info', 'Pomyślnie opuszczono grupę ' . $group->name);
    }

    public function subscribeGroup(int $id): void
    {
        $sub = Auth::user()->groupUsers->where('group_id', $id)->first();
        $sub->subscription = 1;
        $sub->save();
    }

    public function unsubscribeGroup(int $id): void
    {
        $sub = Auth::user()->groupUsers->where('group_id', $id)->first();
        $sub->subscription = 0;
        $sub->save();
    }

    public function transferGroupUser(int $group_id, int $user_id): void
    {
        if (GroupUser::where('group_id', $group_id)->where('group_role_id', GroupRole::OWNER)->first()) {
            $oldOwner = GroupUser::where('group_id', $group_id)->where('group_role_id', GroupRole::OWNER)->first();
            $oldOwner->group_role_id = GroupRole::MODERATOR;
            $oldOwner->save();
        }

        $this->setRole($group_id, $user_id, GroupRole::OWNER);
    }

    public function promoteGroupUser(int $group_id, int $user_id): void
    {
        $this->setRole($group_id, $user_id, GroupRole::MODERATOR);
    }

    public function demoteGroupUser(int $group_id, int $user_id): void
    {
        $this->setRole($group_id, $user_id, GroupRole::MEMBER);
    }

    public function kickGroupUser(int $group_id, int $user_id): void
    {
        $group_user = GroupUser::where('user_id', $user_id)->where('group_id', $group_id)->first();
        $group_user->delete();
    }

}