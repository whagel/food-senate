<?php

namespace App\Services;

use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;

class AdminService
{
    public function banUser(int $id): void
    {
        $user = User::find($id);
        $user->role_id = Role::BANNED;
        $user->banned_at = Carbon::now();
        $user->save();
    }

    public function setDefaultRank(int $id): void
    {
        $user = User::find($id);
        $user->role_id = Role::USER;
        if ($user->banned_at = !null) {
            $user->banned_at = null;
        }
        $user->save();
    }

    public function promoteUser(int $id): void
    {
        $user = User::find($id);
        $user->role_id = Role::ADMIN;
        $user->save();
    }


}