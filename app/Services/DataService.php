<?php

namespace App\Services;

use App\Models\Restaurant;
use App\Models\Type;

class DataService
{

    protected $dishes;

    public function saveDishes($dishes, $restaurant)
    {
        $restaurant->dishes()->saveMany($dishes);
    }

    public function saveIngredients($ingredients, $dish)
    {
        $dish->ingredients()->saveMany($ingredients);
    }
}