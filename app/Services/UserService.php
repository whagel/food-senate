<?php

namespace App\Services;

use App\Http\Requests\StorePassword;
use App\Http\Requests\StoreUser;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserService
{
    public function updateProfile(StoreUser $request_data)
    {
        $user = Auth::user();
        $user->name = $request_data->name;
        $user->description = $request_data->description;
        $user->save();
    }

    public function activate($token, $user)
    {
        if($user->activationKey->token == $token)
        {
            $user->is_activated = 1;
            $user->activationKey->delete();
            $user->save();
        } else {
            return view('/home')->with('error', 'Błąd aktywacji konta.');
        }

        Auth::loginUsingId($user->id);
    }

    public function updatePassword(StorePassword $request): RedirectResponse
    {
        $user = Auth::user();

        if (!(Hash::check($request->post('current-password'), $user->password))) {
            return redirect()->back()->with('error', 'Podane hasło nie pasuje do aktualnego. Spróbuj ponownie.');
        }

        $user->password = bcrypt($request->post('password'));
        $user->save();

        return redirect()->back()->with('success', 'Hasło zostało zmienione.');
    }

    public function updateAvatar(UploadedFile $avatar): RedirectResponse
    {
        $user = Auth::user();

        $name = md5($user->id) . '.' . $avatar->getClientOriginalExtension();
        Storage::putFileAs('public/avatars', $avatar, $name);

        $user->avatar = $name;
        $user->save();

        return redirect()->back()->with('success', 'Avatar został zmieniony.');
    }
}