<?php

namespace App\Services;

use App\Models\Event;
use Carbon\Carbon;

class EventService
{
    public function create($request_data, $id, $user): void
    {
        $event = new Event();
        $event->fill($request_data);
        $event->start_time = Carbon::parse($request_data['start_time']);
        $event->end_time = Carbon::parse($request_data['end_time']);
        $event->group_id = $id;
        $event->user_id = $user->id;
        $event->location = $request_data['location'];
        $event->save();
    }

    public function edit($request_data, Event $event): void
    {
        $event->name = $request_data['name'];
        $event->description = $request_data['description'];
        $event->location = $request_data['location'];
        $event->save();
    }

    public function remove(int $id): void
    {
        $event = Event::find($id);
        $event->delete();
    }
}