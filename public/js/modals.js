$(".modal-user-button").click(function () {
    var user_link = $(this).data("user-link");
    var user_name = $(this).data("user-name");
    var user_email = $(this).data("user-email");
    var user_description = $(this).data("user-description");
    var title = $(this).data("group-title");
    var button_style = $(this).data("button");
    var text = $(this).data("text");

    $("#modalTitle").html(title);
    $(".modal-data-user-name").html("Nazwa: " + user_name);
    $(".modal-data-user-email").html("Email: " + user_email);
    $(".modal-data-user-description").html("Opis: " + user_description);
    $(".modal-user-form").attr("action", user_link);
    $("#post-button").attr("class", "btn " + button_style + " handcursor");
    $("#post-button").html(text);
});


$(".modal-group-button").click(function () {
    var group_link = $(this).data("group-link");
    var group_name = $(this).data("group-name");
    var group_location = $(this).data("group-location");
    var group_users = $(this).data("group-users");
    var title = $(this).data("group-title");
    var button_style = $(this).data("button");
    var text = $(this).data("text");

    $("#modalTitle").html(title);
    $(".modal-data-group-name").html("Nazwa: " + group_name);
    $(".modal-data-group-location").html("Lokalizacja: " + group_location);
    $(".modal-data-group-users").html("Ilość członków: " + group_users);
    $(".modal-group-form").attr("action", group_link);
    $("#post-button").attr("class", "btn " + button_style + " handcursor");
    $("#post-button").html(text);
});