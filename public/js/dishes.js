$(document).ready(function () {
    $(".restaurant-dishes-show").click(function () {
        var button = $(this);
        document.getElementById('dishes-list').innerHTML = "";
        $.ajax({
            method: "GET",
            url: $(".restaurant-dishes-show").val(),
            data: {
                id: button.attr('id')
            }
        })
            .done(function (data) {
                data = $.parseJSON(data);

                $.each(data, function (index) {
                    var tableRef = document.getElementById('dishes-table').getElementsByTagName('tbody')[0];
                    var newRow   = tableRef.insertRow(tableRef.rows.length);
                    newRow.insertCell(0).appendChild(document.createTextNode(data[index].name));
                    newRow.insertCell(1).appendChild(document.createTextNode(data[index].price));
                });
            });
    });
});

