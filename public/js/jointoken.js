$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".token-generate-button").click(function () {
    var link = $(".token-generate-button").data("url");
    $.ajax({
        method: "POST",
        url: link,
        data: {
            expiration: $("#start_time").val(),
            permanent: $('#expirationCheck').is(":checked")
        }
    })
        .done(function (data) {
            $(".token-field").val($(".token-field").data("token-url") + data);
        });

});