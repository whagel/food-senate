<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{

    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned;
            $table->integer('user_id')->unsigned;
            $table->integer('restaurant_id')->unsigned;
        });
    }

    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
