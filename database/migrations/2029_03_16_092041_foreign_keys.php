<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles');
        });

        Schema::table('ingredients', function(Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('ingredient_types')->onDelete('cascade');
            $table->foreign('dish_id')->references('id')->on('dishes')->onDelete('cascade');
        });

        Schema::table('dishes', function(Blueprint $table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');
        });

        Schema::table('restaurants', function(Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });

        Schema::table('events', function(Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('groups', function(Blueprint $table) {
            $table->foreign('group_type_id')->references('id')->on('group_types');
        });

        Schema::table('group_users', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('group_role_id')->references('id')->on('group_roles');
            $table->foreign('group_id')->references('id')->on('groups');
        });

        Schema::table('user_activations', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('invite_tokens', function(Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups');
        });

        Schema::table('restaurant_types', function(Blueprint $table) {
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_types', function(Blueprint $table) {
            $table->dropForeign('restaurant_types_type_id_foreign');
            $table->dropForeign('restaurant_types_restaurant_id_foreign');
        });

        Schema::table('invite_tokens', function(Blueprint $table) {
            $table->dropForeign('invite_tokens_group_id_foreign');
        });

        Schema::table('user_activations', function(Blueprint $table) {
            $table->dropForeign('user_activations_user_id_foreign');
        });

        Schema::table('group_users', function(Blueprint $table) {
            $table->dropForeign('group_users_group_id_foreign');
            $table->dropForeign('group_users_user_id_foreign');
            $table->dropForeign('group_users_group_role_id_foreign');
        });

        Schema::table('groups', function(Blueprint $table) {
            $table->dropForeign('groups_group_type_id_foreign');
        });

        Schema::table('events', function(Blueprint $table) {
            $table->dropForeign('events_user_id_foreign');
            $table->dropForeign('events_group_id_foreign');
        });

        Schema::table('restaurants', function(Blueprint $table) {
            $table->dropForeign('restaurants_event_id_foreign');
        });

        Schema::table('dishes', function(Blueprint $table) {
            $table->dropForeign('dishes_restaurant_id_foreign');
        });

        Schema::table('ingredients', function(Blueprint $table) {
            $table->dropForeign('ingredients_dish_id_foreign');
            $table->dropForeign('ingredients_type_id_foreign');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_role_id_foreign');
        });
    }
}
