<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviteTokensTable extends Migration
{
    public function up()
    {
        Schema::create('invite_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->string('token');
            $table->dateTime('expiration_date')->default(Carbon::now())->nullable();
            $table->boolean('is_permanent')->default(true);
        });
    }

    public function down()
    {
        Schema::dropIfExists('invite_tokens');
    }
}
