<?php

use App\Models\Role;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model.php Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(User::class, function (Faker $faker) {
    $role = Role::$roles[rand(0,sizeof(Role::$roles)-1)];
    $rand = rand(0,3);

    //Zmniejszenie ilości adminów i zbanowanych
    if(($role==Role::BANNED || $role==Role::ADMIN) && $rand != 0){
        $role = Role::USER;
    }

    return [
        'name' => $faker->lastname(),
        'email' => $faker->unique()->safeEmail(),
        'password' => Hash::make('password'),
        'role_id' => $role,
        'is_activated' => true,
        'description' => $faker->sentence(20,true),
    ];
});
