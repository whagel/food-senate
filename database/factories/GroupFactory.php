<?php

use App\Models\Group;
use App\Models\GroupType;
use Faker\Generator as Faker;
/*
|--------------------------------------------------------------------------
| Model.php Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Group::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'location' => $faker->city,
        'group_type_id' => GroupType::$types[rand(0, sizeof(GroupType::$types)-1)],
    ];
});
