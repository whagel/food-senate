<?php

use App\Models\PostCode;
use Illuminate\Database\Seeder;

class PostCodesTableSeeder extends Seeder
{
    public function run()
    {
        $handle = fopen('resources/datasets/post_codes.csv', 'r');

        while ($csvLine = fgetcsv($handle, 10000, ',')) {
            PostCode::firstOrCreate([
                'code' => $csvLine[0],
                'location' => $csvLine[1],
            ]);
        }
    }
}