<?php

use App\Models\Group;
use App\Models\GroupRole;
use App\Models\User;
use Illuminate\Database\Seeder;

class GroupUsersTableSeeder extends Seeder
{
    public function run()
    {
        $grouproles = [GroupRole::MEMBER, GroupRole::MODERATOR];
        foreach (User::all() as $user) {
            $groups = Group::inRandomOrder()->take(Group::all()->count())->get()->random(rand(0, 5));
            if (!$groups->isEmpty()) {
                foreach ($groups as $group) {
                    $index = rand(0, sizeof($grouproles)-1);
                    $group->users()->attach($user->id, ['group_role_id' => $grouproles[$index]]);
                }
            }
        }
    }
}
