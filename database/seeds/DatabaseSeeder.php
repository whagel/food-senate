<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GroupTypesTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(GroupRolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(GroupUsersTableSeeder::class);
        $this->call(IngredientTypesTableSeeder::class);
        //$this->call(PostCodesTableSeeder::class);
    }
}
