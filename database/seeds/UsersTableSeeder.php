<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        factory(User::class, 50)->create();
        User::firstOrCreate(['name' => 'Admin'],
            [
                'email' => 'admin@example.com',
                'password' => Hash::make('123123'),
                'is_activated' => 1,
                'role_id' => Role::SUPERADMIN,
            ]);
    }
}