<?php

use App\Models\GroupRole;
use Illuminate\Database\Seeder;

class GroupRolesTableSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            GroupRole::OWNER => 'OWNER',
            GroupRole::MODERATOR => 'MODERATOR',
            GroupRole::MEMBER => 'MEMBER',
        ];

        foreach ($roles as $key => $value) {
            GroupRole::firstOrCreate([
                'id' => $key,
                'name' => $value,
            ]);
        }
    }
}
