<?php

use App\Models\GroupType;
use Illuminate\Database\Seeder;

class GroupTypesTableSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            GroupType::PUBLIC => 'PUBLIC',
            GroupType::PRIVATE => 'PRIVATE',
        ];

        foreach ($roles as $key => $value) {
            GroupType::firstOrCreate([
                'id' => $key,
                'name' => $value,
            ]);
        }
    }
}
