<?php

use App\Models\IngredientType;
use Illuminate\Database\Seeder;

class IngredientTypesTableSeeder extends Seeder
{
    public function run()
    {
        IngredientType::firstOrCreate([
            'name' => 'jedzenie albo picie',
        ]);
    }
}
