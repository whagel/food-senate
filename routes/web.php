<?php
//Kazdy moze uzyc tego routa

Route::get('/', 'HomeController@getIndexView')->name('home');
Route::get('/activation/{id}/{token}', 'UserController@activate')->where('id', '[0-9]+')->name('user.activate');

Auth::routes();

//Kazdy, zbanowany lub nie, moze sie wylogowac
Route::group(['middleware' => 'auth'], function () {
});

//Co moga robic niezbanowani uzytkownicy
Route::group(['middleware' => ['auth', 'not_banned']], function () {

    Route::get('/groups', 'GroupController@publicList')->name('public.group.list');
    Route::get('/group/{id}', 'GroupController@show')->where('id', '[0-9]+')->name('group');
    Route::get('/mygroups', 'GroupController@groups')->name('user.groups');

    Route::post('/group/create', 'GroupExistenceController@create')->name('group.create.post');
    Route::get('/group/create', 'GroupExistenceController@createFrom')->name('group.create');

    Route::post('/group/{id}/remove', 'GroupExistenceController@remove')->where('id', '[0-9]+')->name('group.remove');
    Route::get('/group/{id}/remove', 'GroupExistenceController@removeCheck')->where('id', '[0-9]+')->name('group.remove.check');

    Route::post('/group/{id}/edit', 'GroupExistenceController@edit')->where('id', '[0-9]+')->name('group.edit.post');
    Route::get('/group/{id}/edit', 'GroupExistenceController@editForm')->where('id', '[0-9]+')->name('group.edit');

    Route::post('/group/{id}/user/transfer/{user_id}', 'GroupPermissionsController@groupUserTransfer')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.transfer');
    Route::get('/group/{id}/user/transfer/{user_id}', 'GroupPermissionsController@groupUserTransferCheck')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.transfer.check');

    Route::post('/group/{id}/user/promote/{user_id}', 'GroupPermissionsController@groupUserPromote')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.promote');
    Route::get('/group/{id}/user/promote/{user_id}', 'GroupPermissionsController@groupUserPromoteCheck')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.promote.check');

    Route::post('/group/{id}/user/demote/{user_id}', 'GroupPermissionsController@groupUserDemote')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.demote');
    Route::get('/group/{id}/user/demote/{user_id}', 'GroupPermissionsController@groupUserDemoteCheck')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.demote.check');

    Route::post('/group/{id}/user/remove/{user_id}', 'GroupRoamController@groupUserKick')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.kick');
    Route::get('/group/{id}/user/remove/{user_id}', 'GroupRoamController@groupUserKickCheck')->where('id', '[0-9]+')->where('user_id', '[0-9]+')->name('group.kick.check');

    Route::post('/group/{id}/join', 'GroupRoamController@join')->where('id', '[0-9]+')->name('group.join');
    Route::get('/group/{id}/join', 'GroupRoamController@joinCheck')->where('id', '[0-9]+')->name('group.join.check');

    Route::post('/group/{id}/leave', 'GroupRoamController@leave')->where('id', '[0-9]+')->name('group.leave');
    Route::get('/group/{id}/leave', 'GroupRoamController@leaveCheck')->where('id', '[0-9]+')->name('group.leave.check');

    Route::post('/group/{id}/store/token', 'GroupInviteController@storeToken')->where('id', '[0-9]+')->name('group.store.token');
    Route::get('/join/{token}', 'GroupRoamController@joinByToken')->name('group.join.token');

    Route::get('/group/{id}/event/create', 'EventExistenceController@createForm')->where('id', '[0-9]+')->name('event.create.form');
    Route::post('/group/{id}/event/create', 'EventExistenceController@create')->where('id', '[0-9]+')->name('event.create.post');

    Route::get('/event/{id}', 'EventController@show')->where('id', '[0-9]+')->name('event');
    Route::get('/event/{id}/dishes', 'EventController@dishes')->where('id', '[0-9]+')->name('dishes.show');

    Route::get('/event/{id}/edit', 'EventExistenceController@editForm')->where('id', '[0-9]+')->name('event.edit.form');
    Route::post('/event/{id}/edit', 'EventExistenceController@edit')->where('id', '[0-9]+')->name('event.edit.post');

    Route::get('/event/{id}', 'EventController@show')->where('id', '[0-9]+')->name('event.show');
    Route::get('/event/{id}/remove', 'EventExistenceController@removeForm')->where('id', '[0-9]+')->name('event.remove.form');
    Route::post('/event/{id}/remove', 'EventExistenceController@remove')->where('id', '[0-9]+')->name('event.remove');
    Route::post('/event/{event_id}/vote/{restaurant_id}', 'VoteController@storeVote')->where('event_id', '[0-9]+')->where('restaurant_id', '[0-9]+')->name('vote.post');
    Route::get('/event/{event_id}/vote/{restaurant_id}', 'VoteController@storeVote')->where('event_id', '[0-9]+')->where('restaurant_id', '[0-9]+')->name('vote.post');
    Route::post('/event/{event_id}/finish', 'VoteController@countVotes')->where('event_id', '[0-9]+')->name('event.finish.post');
    Route::get('/event/{event_id}/finish', 'VoteController@countVotes')->where('event_id', '[0-9]+')->name('event.finish.post');

    Route::get('/profile/{id?}', 'UserController@getProfileView')->where('id', '[0-9]+')->name('user.profile');
    Route::get('/editProfile', 'UserEditController@getProfileEditForm')->name('user.edit.profile');
    Route::get('/editPassword', 'UserEditController@getPasswordEditForm')->name('user.edit.password');

    Route::post('/edit/profile', 'UserEditController@editProfile')->name('user.edit.profile.post');
    Route::post('/edit/password', 'UserEditController@updatePasswordForm')->name('user.edit.password.post');
    Route::post('/edit/avatar', 'UserEditController@updateAvatarForm')->name('user.edit.avatar.post');

    Route::get('subscribe/{id}', 'GroupController@subscribe')->where('id', '[0-9]+')->name('subscribe');
    Route::get('unsubscribe/{id}', 'GroupController@unsubscribe')->where('id', '[0-9]+')->name('unsubscribe');

    Route::get('/restaurant/{id}/dishes', 'RestaurantController@dishes')->where('id', '[0-9]+')->name('dishes.show');
});

//Co moga robic admini + wszystko to, co zwykli uzytkownicy
Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/admin/panel', 'AdminViewsController@getGroupsListView')->name('admin.panel');
    Route::get('/admin/groups', 'AdminViewsController@getGroupsListView')->name('admin.groups');
    Route::get('/admin/users', 'AdminViewsController@getUsersListView')->name('admin.users');
    Route::get('/admin/banned', 'AdminViewsController@getBannedUsersListView')->name('admin.banned');
    Route::post('/user/{id}/ban', 'AdminController@userBan')->where('id', '[0-9]+')->name('user.ban');
    Route::get('/user/{id}/ban', 'AdminController@userBanCheck')->where('id', '[0-9]+')->name('user.ban.check');
    Route::post('/user/{id}/unban', 'AdminController@userUnban')->where('id', '[0-9]+')->name('user.unban');
    Route::get('/user/{id}/unban', 'AdminController@userUnbanCheck')->where('id', '[0-9]+')->name('user.unban.check');
});

//Co moze robic superadmin + admin + zwykly user
Route::group(['middleware' => ['auth', 'superadmin']], function () {
    Route::get('/admin/admins', 'AdminViewsController@getAdminsListView')->name('admin.admins');
    Route::post('/user/{id}/promote', 'AdminPermissionController@userPromote')->where('id', '[0-9]+')->name('user.promote');
    Route::get('/user/{id}/promote', 'AdminPermissionController@userPromoteCheck')->where('id', '[0-9]+')->name('user.promote.check');
    Route::post('/user/{id}/demote', 'AdminPermissionController@userDemote')->where('id', '[0-9]+')->name('user.demote');
    Route::get('/user/{id}/demote', 'AdminPermissionController@userDemoteCheck')->where('id', '[0-9]+')->name('user.demote.check');
});

//scraper
Route::get('/scraper/{id}', 'WebScraperController@getRestaurants')->where('id', '[0-9]+')->name('scraper');
Route::get('/scraperfood/{id}', 'WebScraperController@getDishes')->where('id', '[0-9]+')->name('scraperfood');
Route::get('/scrapertypes/{id}', 'WebScraperController@getRestaurantTypes')->where('id', '[0-9]+')->name('scrapertype');