@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Edytuj grupę
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('group.edit', $group->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nazwa grupy</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $group->name }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="location"
                                       class="col-md-4 col-form-label text-md-right">Lokalizacja</label>
                                <div class="col-md-6">
                                    <input id="location" type="text" class="form-control" name="location"
                                           value="{{ $group->location }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="privateCheck"
                                       class="col-md-4 col-form-label text-md-right">Widoczność grupy</label>
                                <div class="col-md-6 text-md-left">
                                    <select id="privateCheck" name="group_type_id" class="form-control">
                                        <option value="1" @if ($group->group_type_id  == 1) selected="selected" @endif>
                                            Publiczna
                                        </option>
                                        <option value="2" @if ($group->group_type_id == 2) selected="selected" @endif>
                                            Prywatna
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Zapisz
                                    </button>

                                    <a href="{{ URL::previous() }}" class="btn btn-secondary">Anuluj</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                @if($group->isOwner(Auth::user()) ||  Auth::user()->isAdmin())
                    <div class="card">
                        <div class="card-header">
                            Zarządzaj użytkownikami
                        </div>
                        <div class="card-body">

                            <table class="table table-bordered border-dark">
                                <thead>
                                <tr>
                                    <th class="text-center">Użytkownik</th>
                                    <th>Właściciel</th>
                                    <th>Moderator</th>
                                    <th>Usuń z grupy</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($group->groupUsers as $group_user)
                                    <tr>
                                        <td class="text-left break align-middle py-0" style="min-width:250px" >
                                            <img class="img-thumbnail small-avatar-img"
                                                 src="{{ $group_user->user->getAvatarPath() }}">
                                            <a href="{{ route('user.profile', $group_user->user->id) }}"
                                               class="btn btn-outline-primary border-0 form-inline my-0">{{ $group_user->user->name }}</a>
                                        </td>
                                        <td> @if(!$group->isOwner($group_user->user))
                                                <a href="{{ route('group.transfer.check', [$group->id, $group_user->user->id]) }}"
                                                   class="btn btn-info">Przekaż</a>
                                            @endif</td>
                                        <td> @if(!$group->isModerator($group_user->user) && !$group->isOwner($group_user->user))
                                                <a href="{{ route('group.promote.check', [$group->id, $group_user->user->id]) }}"
                                                   class="btn btn-success">Nadaj</a>
                                            @elseif(!$group->isOwner($group_user->user))
                                                <a href="{{ route('group.demote.check', [$group->id, $group_user->user->id]) }}"
                                                   class="btn btn-outline-success">Odbierz</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('group.kick.check', [$group->id, $group_user->user->id]) }}"
                                               class="btn btn-outline-danger">Wyrzuć</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
