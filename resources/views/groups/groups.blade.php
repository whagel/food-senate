@extends('root')

@section('content')

    <div class="group-cards">
        <div class="row justify-content-center groups-single-page">
            @foreach($groups as $group)
                <div class="card card-max border-0 col-md-3 py-0 px-0 my-2 mx-2 text-nowrap">

                    <div class="card-header leave-fill text-center px-0 py-1 border-0">
                        <a class="w-100 white-link" href="{{ route('group', $group->id) }}">
                            {{$group->name}}
                        </a>
                    </div>
                    <div class="card-body leave-white text-center px-0 py-1 my-1 leave-border-dashed">
                        <p class="w-100 text-left px-2 my-0">Lokalizacja:&nbsp;{{ $group->location }}</p>
                        <p class="w-100 text-right px-2 mb-0 mt-2">
                            <small>Ilość członków:&nbsp;{{ $group->users->count() }}</small>
                        </p>
                    </div>
                    <div class="card-footer leave-fill text-center px-0 border-0 py-1">
                        <button type="button" class="btn leave-white handcursor modal-group-button"
                                data-toggle="modal"
                                data-group-title="Czy chcesz opuścić grupę"
                                data-group-link="{{ route('group.leave', $group->id) }}"
                                data-group-name="{{ $group->name }}"
                                data-group-location="{{ $group->location }}"
                                data-group-users="{{ $group->users->count() }}"
                                data-text="Opuść"
                                data-button="btn-warning"
                                data-target=".modal-group">Opuść
                        </button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection

@include('modals.group')

@section('scripts')
    <script src="{{ URL::asset('js/modals.js')  }}"></script>
    <script type="text/javascript">
        $('ul.pagination').hide();
        $('.group-cards').infiniteScroll({
            path: '.pagination li.active + li a',
            append: '.groups-single-page',
            hideNav: '.pagination',
            status: '.scroller-status',
            prefill: true,
            callback: function () {
                $('ul.pagination').remove();
            }
        });
    </script>
@endsection


