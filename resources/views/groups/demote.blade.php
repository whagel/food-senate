@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Czy odebrać użytkownikowi <strong> {{ $group_user->first()->user->name }} </strong> rangę moderatora grupy <strong> {{ $group->name }}</strong>?
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('group.demote', [$group->id, $group_user->first()->user_id]) }}">
                            @csrf
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success handcursor">
                                    Degraduj
                                </button>

                                <a href="{{ URL::previous() }}" class="btn btn-outline-primary">Anuluj</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
