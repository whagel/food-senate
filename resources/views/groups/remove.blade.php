@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Czy na pewno checesz usunąć grupę:
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('group.remove', $group->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nazwa grupy:</label>

                                <label id="name" class="col-md-4 col-form-label text-md-left">{{ $group->name }}</label>
                            </div>
                            <div class="form-group row">
                                <label for="location"
                                       class="col-md-4 col-form-label text-md-right">Lokalizacja:</label>

                                <label id="location"
                                       class="col-md-4 col-form-label text-md-left">{{ $group->location }}</label>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-danger">
                                        Usuń
                                    </button>
                                    <a href="{{ URL::previous() }}" class="btn btn-primary">Anuluj</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
