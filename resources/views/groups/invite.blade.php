@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    Token:
                    <br/>{{ url('/').'/group/join/'.$group->inviteToken->token }}
                    <form method="POST" action=" {{ route('group.store.token', $group->id) }}">
                        @csrf

                        <button type="submit" class="btn btn-danger">
                            Wygeneruj nowy
                        </button>
                        <a class="btn btn-outline-warning" href="{{ route('home')}}">Cofnij</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection




<div class="group_cards">
    <div class="card-deck justify-content-center groups-single-page">
        @foreach($groups as $group)
            <div class="group_card px-0 col-md-3 mx-2 mt-2 post">
                <div class="card content-background w-100 h-100 mx-0 px-0 border-0 rounded-0">
                    <div class="card-header content-background text-center py-1 border-0">
                        <a href="{{ route('group', $group->id) }}" class="panel ">
                            <div>
                                <p class="border-bottom-1">{{ $group->name }}</p>
                            </div>
                        </a>
                    </div>
                    <div class="card-body content-background border-0 py-1">
                        <div class="w-100 h-100">
                            <small>Lokalizacja:</small>
                            <br/>{{ $group->location }}
                            <br/>adssad
                            <br/>adssad
                        </div>
                    </div>
                    <div class="card-footer content-background text-center align-text-bottom border-0 border-bottom-1 py-0">
                        <p class="text-md-right">
                            <small>Ilość członków:&nbsp;{{ $group->users->count() }}</small>
                        </p>
                    </div>
                    <div class="card-footer content-background text-center align-text-bottom">
                        @if(!$group->isMember(Auth::user()))

                            <button type="button" class="btn btn-success handcursor modal-group-button"
                                    data-toggle="modal"
                                    data-group-title="Czy chcesz dołączyć do grupy"
                                    data-group-link="{{ route('group.join', $group->id) }}"
                                    data-group-name="{{ $group->name }}"
                                    data-group-location="{{ $group->location }}"
                                    data-group-users="{{ $group->users->count() }}"
                                    data-text="Dołącz"
                                    data-button="btn-success"
                                    data-target=".modal-group">Dołącz
                            </button>
                        @else
                            <button type="button" class="btn btn-warning handcursor modal-group-button"
                                    data-toggle="modal"
                                    data-group-title="Czy chcesz opuścić grupę"
                                    data-group-link="{{ route('group.leave', $group->id) }}"
                                    data-group-name="{{ $group->name }}"
                                    data-group-location="{{ $group->location }}"
                                    data-group-users="{{ $group->users->count() }}"
                                    data-text="Opuść"
                                    data-button="btn-warning"
                                    data-target=".modal-group">Opuść
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="scroller-status">
        <div class="infinite-scroll-request loader-ellips">
            ...
        </div>
        <p class="infinite-scroll-last">Koniec zawartości</p>
        <p class="infinite-scroll-error">Brak treści do wczytania</p>
    </div>

    <p class="pagination">
        {{ $groups->links() }}
    </p>
</div>
<br/>

