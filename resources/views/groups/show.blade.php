@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(!$group->hasOwner() && ($group->isMember(Auth::user()) || Auth::user()->isAdmin()))
                    <div class="alert alert-warning">
                        <i class="fas fa-exclamation-triangle"></i><strong>&nbsp;Uwaga!</strong>
                        <br/>Grupa nie ma właściciela.
                        <br/>Proszę skontaktować się z administratorem w celu rozwiązania tego problemu.
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <div id="container">
                            <div class="float-left">
                                <label> <strong>Informacje o grupie {{ $group->name }}</strong></label>
                                <br/>
                                <label><strong>Lokalizacja: </strong>{{ $group->location }}</label>
                            </div>

                            <div class="float-right">
                                @if($group->isOwner(Auth::user()) || $group->isModerator(Auth::user()) ||  Auth::user()->isAdmin())
                                    <td><a href="{{ route('group.edit', $group->id) }}"
                                           class="btn btn-outline-primary">Edytuj</a>
                                    </td>
                                @endif
                                @if($group->isModerator(Auth::user()) || $group->isOwner(Auth::user()))
                                    <button type="button" class="btn btn-primary handcursor" data-toggle="modal"
                                            data-target=".modal-invite">Zaproś
                                    </button>
                                @endif
                                @if (!$member)
                                    <td>
                                        <button type="button" class="btn btn-success handcursor modal-group-button"
                                                data-toggle="modal"
                                                data-group-title="Czy chcesz dołączyć do grupy"
                                                data-group-link="{{ route('group.join', $group->id) }}"
                                                data-group-name="{{ $group->name }}"
                                                data-group-location="{{ $group->location }}"
                                                data-group-users="{{ $group->users->count() }}"
                                                data-text="Dołącz"
                                                data-button="btn-success"
                                                data-target=".modal-group">Dołącz
                                        </button>
                                    </td>
                                @else
                                    <td>
                                        <button type="button" class="btn btn-warning handcursor modal-group-button"
                                                data-toggle="modal"
                                                data-group-title="Czy chcesz opuścić grupę"
                                                data-group-link="{{ route('group.leave', $group->id) }}"
                                                data-group-name="{{ $group->name }}"
                                                data-group-location="{{ $group->location }}"
                                                data-group-users="{{ $group->users->count() }}"
                                                data-text="Opuść"
                                                data-button="btn-warning"
                                                data-target=".modal-group">Opuść
                                        </button>
                                    </td>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="card-header">
                        <div class="float-left">
                            @if($group->isMember(Auth::user()))
                                <label><strong>Wydarzenia</strong></label>
                        </div>

                        <div class="float-left">
                            @if(!$group->isSubscribed($group->id, Auth::user()))
                                <a href="{{ route('subscribe', $group->id) }}"
                                   class="btn btn-info btn-sm">Otrzymuj powiadomienia</a>
                            @else
                                <a href="{{ route('unsubscribe', $group->id) }}"
                                   class="btn btn-outline-info btn-sm">✘ Anuluj powiadomienia</a>
                            @endif
                        </div>
                        <div class="float-right">
                            <a class="btn btn-outline-primary"
                               href="{{ route('event.create.form', $group->id) }}">Dodaj wydarzenie</a>
                            @endif
                        </div>
                    </div>

                    <table class="table table-bordered border-dark">
                        <thead>
                        <tr>
                            <th>Nazwa wydarzenia</th>
                            <th>Rozpoczęcie</th>
                            <th>Zakończenie</th>
                            <th colspan="2" class="text-center">Szczegóły</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach ($group->events as $event)
                            <tr>
                                <td><a href="{{ route('event.show', $event->id) }}"
                                       class="btn btn-outline-primary border-0">{{ $event->name }}</a></td>
                                <td>{{ $event->start_time }}</td>
                                <td>{{ $event->end_time }}</td>
                                <td><a href="{{ route('event.show', $event->id) }}" class="btn btn-outline-success">Zobacz</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="card-header">
                        <label>Członkowie grupy:</label>
                    </div>

                    <table class="table table-bordered border-dark mb-0">
                        @foreach ($group->users as $group_user)
                            @if(!$group_user->isBanned())
                                <tr>
                                    <td class="py-0"><img class="img-thumbnail small-avatar-img"
                                                          src="{{ $group_user->getAvatarPath() }}">
                                        <a href="{{ route('user.profile', $group_user->id) }}"
                                           class="btn btn-outline-primary border-0 form-inline my-0">{{ $group_user->name }}</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        @if($group->isOwner(Auth::user()))
                            <tr>
                                <td class="py-0 text-right">
                                    <button type="button"
                                            class="btn btn-sm btn-danger handcursor modal-group-button"
                                            data-toggle="modal"
                                            data-group-title="Czy na pewno chcesz usunąć grupę"
                                            data-group-link="{{ route('group.remove', $group->id) }}"
                                            data-group-name="{{ $group->name }}"
                                            data-group-location="{{ $group->location }}"
                                            data-group-users="{{ $group->users->count() }}"
                                            data-text="Usuń"
                                            data-button="btn-danger"
                                            data-target=".modal-group">Usuń grupę
                                    </button>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('modals.invite')
@include('modals.group')

@section('scripts')
    <script src="{{ URL::asset('js/jointoken.js')  }}"></script>
    <script src="{{ URL::asset('js/modals.js')  }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#expirationdatepicker').datetimepicker({
                format: "DD.MM.YYYY HH:mm",
            });
        });
        $("#start_time").val($("#start_time").data("expiration"));

    </script>
    <script>
        function copyLink() {

            var copyText = document.getElementById("token");
            copyText.select();
            document.execCommand("Copy");
        }
    </script>
@endsection
