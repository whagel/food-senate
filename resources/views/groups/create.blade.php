@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Stwórz grupę
                    </div>
                    <div class="card-body">
                        @if (Auth::user())
                            <form method="POST" action=" {{ route('group.create.post') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="name"
                                           class="col-md-4 col-form-label text-md-right">Nazwa grupy</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="location"
                                           class="col-md-4 col-form-label text-md-right">Lokalizacja</label>
                                    <div class="col-md-6">
                                        <input id="location" type="text" class="form-control"
                                               name="location" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="privateCheck"
                                           class="col-md-4 col-form-label text-md-right">Widoczność grupy</label>
                                    <div class="col-md-6 text-md-left">
                                        <select id="privateCheck" name="group_type_id" class="form-control">
                                            <option value="{{ $public }}">
                                                Publiczna
                                            </option>
                                            <option value="{{ $private }}">
                                                Prywatna
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-danger">
                                            Utwórz grupę
                                        </button>

                                        <a class="btn btn-outline-warning" href="{{ route('home')}}">Anuluj</a>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div class="btn btn-danger">
                                Aby utworzyć grupę, musisz być zalogowany.</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
