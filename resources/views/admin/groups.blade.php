@extends('admin.panel')
@section('admin.content')
    <table class="table table-bordered border-dark">
        <thead>
        <tr>
            <th style="width: 5%">Id</th>
            <th>Nazwa</th>
            <th>Lokalizacja</th>
            <th>Typ</th>
            <th class="text-center">Opcje</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($groups as $group)

            <tr>
                <td class="text-left align-middle py-0"> {{ $group->id }}</td>
                <td class="text-left align-middle py-0">
                    <a href="{{ route('group', $group->id) }}"
                       class="btn btn-outline-primary border-0 my-0 py-0">{{ $group->name }}</a>
                </td>
                <td class="align-middle py-0"> {{ $group->location }}</td>
                <td class="text-center align-middle py-0">
                    @if ($group->isPublic())
                        <i class="fas fa-eye"></i>
                    @else
                        <i class="fas fa-eye-slash"></i>
                    @endif
                </td>
                <td class="align-middle  py-1 text-center">
                    <a href="{{ route('group.edit', $group->id) }}"
                       class="btn btn-primary my-0">Edytuj</a>
                    <button type="button" class="btn btn-outline-danger handcursor modal-group-button"
                            data-toggle="modal"
                            data-group-title="Czy na pewno chcesz usunąć grupę"
                            data-group-link="{{ route('group.remove', $group->id) }}"
                            data-group-name="{{ $group->name }}"
                            data-group-location="{{ $group->location }}"
                            data-group-users="{{ $group->users->count() }}"
                            data-text="Usuń"
                            data-button="btn-danger"
                            data-target=".modal-group">Usuń grupę
                    </button>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td class="justify-content-center" colspan="5">
                <div class="pagination center">
                    {{ $groups->links() }}
                </div>
            </td>
        </tr>
        </tfoot>
    </table>
@endsection

@include('modals.group')

@section('scripts')
    <script src="{{ URL::asset('js/modals.js')  }}"></script>
@endsection