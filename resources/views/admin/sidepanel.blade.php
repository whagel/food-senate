<table class="table table-bordered">
    <thead>
    <tr>
        <th colspan="2" class="text-center">Menu</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <a href="{{ route('admin.groups') }}" class="btn btn--light form-inline text-dark"><i class="fas fa-users"></i>&nbsp;Grupy</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="{{ route('admin.users') }}" class="btn btn--light form-inline text-dark"><i class="fas fa-user"></i>&nbsp;Użytkownicy</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="{{ route('admin.banned') }}" class="btn btn--light form-inline text-dark"><i
                        class="fas fa-user-times"></i>&nbsp;Zbanowani</a>
        </td>
    </tr>
    @if(Auth::user()->isSuperAdmin())
        <tr>
            <td>
                <a href="{{ route('admin.admins') }}" class="btn btn--light form-inline text-dark"><i
                            class="fab fa-empire"></i>&nbsp;Administratorzy</a>
            </td>
        </tr>
    @endif
    </tbody>
</table>