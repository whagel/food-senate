@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 ">
                <div class="card">
                    <div class="card-header text-center">
                        <label>Panel administracyjny</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mx-0 px-0">
            <div class="col-lg-2 mx-0 px-0 bg-light">
                @include('admin.sidepanel')

            </div>
            <div class="col-lg-10 mx-0 px-0">
                <div class="card">
                    <div class="card-header">
                        @yield('admin.content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection