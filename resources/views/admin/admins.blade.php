@extends('admin.panel')
@section('admin.content')
    <table class="table table-bordered border-dark">
        <thead>
        <tr>
            <th style="width: 5%">Id</th>
            <th style="width: 10%" class="break">Nazwa</th>
            <th style="width: 35%">Adres Email</th>
            <th style="width: 14%">Rola</th>
            <th>Opcje</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                <td class="text-left align-middle py-0"> {{ $user->id }}</td>
                <td class="text-left break align-middle py-0"> {{ wordwrap($user->name, 13, "-\n", TRUE) }}</td>
                <td class="text-left align-middle py-0"> {{ $user->email }}</td>
                <td class="text-center align-middle py-0 ">{{ __('messages.' . $user->role->name) }}</td>
                <td class="align-middle  py-1">
                    <div class="justify-content-start  text-center">
                        <button type="button" class="btn btn-warning handcursor modal-user-button"
                                data-toggle="modal"
                                data-group-title="Czy chcesz zdegradować użytkownika"
                                data-user-link="{{ route('user.demote', $user->id) }}"
                                data-user-name="{{ $user->name }}"
                                data-user-email="{{ $user->email }}"
                                data-user-description="{{ $user->description }}"
                                data-text="Degraduj"
                                data-button="btn-warning"
                                data-target=".modal-user">Degraduj
                        </button>
                        <button type="button" class="btn btn-danger handcursor modal-user-button"
                                data-toggle="modal"
                                data-group-title="Czy chcesz zbanować użytkownika"
                                data-user-link="{{ route('user.ban', $user->id) }}"
                                data-user-name="{{ $user->name }}"
                                data-user-email="{{ $user->email }}"
                                data-user-description="{{ $user->description }}"
                                data-text="Banuj"
                                data-button="btn-danger"
                                data-target=".modal-user">Banuj
                        </button>

                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td class="justify-content-center" colspan="5">
                <div class="pagination center">
                    {{ $users->links() }}
                </div>
            </td>
        </tr>
        </tfoot>
    </table>
@endsection

@include('modals.user')

@section('scripts')
    <script src="{{ URL::asset('js/modals.js')  }}"></script>
@endsection