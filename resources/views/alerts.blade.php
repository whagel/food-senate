@foreach($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <i class="fas fa-exclamation-triangle"></i>
        <strong>Błąd!</strong> {{ $error }}
    </div>
@endforeach

@if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        <strong>Błąd!</strong> {{ Session::get('error') }}
    </div>
@endif

@if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        <strong>Sukces!</strong> {{ Session::get('success') }}
    </div>
@endif

@if(Session::has('info'))
    <div class="alert alert-info" role="alert">
        <i class="fas fa-exclamation-circle"></i>
        <strong>Informacja.</strong> {{ Session::get('info') }}
    </div>
@endif