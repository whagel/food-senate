<div class="modal fade modal-restaurant" id="modalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table-bordered" id="dishes-table">
                    <thead>
                        <th>Nazwa</th>
                        <th>Składniki</th>
                    </thead>

                    <tbody id="dishes-list">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <form class="modal-group-form" method="POST">
                    @csrf
                    <button type="button" class="btn btn-secondary handcursor" data-dismiss="modal">Zamknij</button>
                </form>
            </div>
        </div>
    </div>
</div>