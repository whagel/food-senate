<div class="modal fade modal-invite" id="modalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <!-- Tworzenie linku z zaproszeniem -->
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Zapraszanie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div><br/></div>
            <div class="form-group row justify-content-start">
                <label for="start_time" class="col-md-4 col-form-label text-md-right">Data wygaśnięcia</label>
                <div class="col-md-6">
                    <div class="input-group date" id="expirationdatepicker"
                         data-target-input="nearest">
                        <input type="text" id="start_time" class="form-control datetimepicker-input"
                               name="start_time" data-expiration="{{ $date }}"
                               data-target="#expirationdatepicker"/>
                        <span class="input-group-append" id="basic-addon2">
                                            <div class="input-group-append" data-target="#expirationdatepicker"
                                                 data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row justify-content-start">
                <label class="form-check-inline col-md-4 col-form-label" for="expirationCheck" style="padding-left:3em">Link
                    nie
                    wygasa</label>
                @if($group->inviteToken->isPermanent())
                    <input class="form-check-inline" type="checkbox" id="expirationCheck" checked="checked">
                @else
                    <input class="form-check-inline" type="checkbox" id="expirationCheck">
                @endif
            </div>
            <div class="modal-body">
                <div class="form-group row justify-content-center">
                    <input type="text" data-token-url="{{ url('/').'/join/' }}"
                           value="{{ url('/').'/join/'.$group->inviteToken->token }}"
                           id="token" class="input-group token-field form-control form-inline col-9"
                           readonly/>
                    <button class="btn btn-primary handcursor form-inline" onclick="copyLink()">Kopiuj</button>

                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-secondary handcursor" data-dismiss="modal">Cofnij</button>
                <a data-group-id="{{ $group->id }}" data-url="{{ route("group.store.token", $group->id) }}"
                   href="#" class="token-generate-button btn btn-primary form-inline my-0">Wygeneruj nowy link</a>
            </div>
        </div>
    </div>
</div>