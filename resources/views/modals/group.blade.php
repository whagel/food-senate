<div class="modal fade modal-group" id="modalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6 class="modal-data-group-name"></h6>
                <h6 class="modal-data-group-location"></h6>
                <h6 class="modal-data-group-users"></h6>
            </div>
            <div class="modal-footer">
                <form class="modal-group-form" method="POST">
                    @csrf
                    <button type="button" class="btn btn-secondary handcursor" data-dismiss="modal">Anuluj</button>
                    <button type="submit" id="post-button" ></button>
                </form>
            </div>
        </div>
    </div>
</div>