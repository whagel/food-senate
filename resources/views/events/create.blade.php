@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Stwórz wydarzenie
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('event.create.post', $group->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nazwa wydarzenia</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-right">Opis</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" id="description" name="description"
                                              placeholder="Opis wydarzenia..."></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="location"
                                       class="col-md-4 col-form-label text-md-right">Lokalizacja</label>

                                <div class="col-md-6">
                                    <input id="location" type="text" class="form-control" name="location"
                                           value="{{ $group->location }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="start_time" class="col-md-4 col-form-label text-md-right">Start</label>
                                <div class="col-md-6">
                                    <div class="input-group date" id="firstdatepicker"
                                         data-target-input="nearest">
                                        <input type="text" id="start_time" class="form-control datetimepicker-input"
                                               name="start_time"
                                               data-target="#firstdatepicker"/>
                                        <span class="input-group-append" id="basic-addon2">
                                            <div class="input-group-append" data-target="#firstdatepicker"
                                                 data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="end_time" class="col-md-4 col-form-label text-md-right">Koniec</label>
                                <div class="col-md-6">
                                    <div class="input-group date" id="seconddatapicker"
                                         data-target-input="nearest">
                                        <input type="text" id="end_time" class="form-control datetimepicker-input"
                                               name="end_time"
                                               data-target="#seconddatapicker"/>
                                        <span class="input-group-append" id="basic-addon2">
                                            <div class="input-group-append" data-target="#seconddatapicker"
                                                 data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            @section('scripts')
                                <script type="text/javascript">
                                    $(function () {
                                        $('#firstdatepicker').datetimepicker({
                                            format: "DD.MM.YYYY HH:mm"
                                        });
                                        $('#seconddatapicker').datetimepicker({
                                            format: "DD.MM.YYYY HH:mm",
                                            useCurrent: false
                                        });
                                        $("#firstdatepicker").on("change.datetimepicker", function (e) {
                                            $('#seconddatapicker').datetimepicker('minDate', e.date);
                                        });
                                        $("#seconddatapicker").on("change.datetimepicker", function (e) {
                                            $('#firstdatepicker').datetimepicker('maxDate', e.date);
                                        });
                                    });
                                </script>
                            @endsection
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-danger">
                                        Utwórz wydarzenie
                                    </button>

                                    <a class="btn btn-outline-primary" href="{{ URL::previous() }}">Anuluj</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection