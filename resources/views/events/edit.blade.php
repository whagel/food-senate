@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Stwórz wydarzenie
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('event.edit.post', $event->id)}}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nazwa wydarzenia</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" value="{{  $event->name }}" name="name" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-right">Opis</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" id="description" name="description"
                                              placeholder="Opis wydarzenia...">{{ $event->description }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="location"
                                       class="col-md-4 col-form-label text-md-right">Lokalizacja</label>

                                <div class="col-md-6">
                                    <input id="location" type="text" class="form-control" value="{{  $event->location }}" name="location" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-danger">
                                        Edytuj
                                    </button>

                                    <a class="btn btn-outline-primary" href="{{ URL::previous() }}">Anuluj</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
