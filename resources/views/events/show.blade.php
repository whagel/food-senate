@extends('root')
@extends('modals.dishes')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <label><strong>Informacje o wydarzeniu {{ $event->name }}</strong></label>
                        <div style="float:right">
                            <a class="btn btn-success" href="{{ URL::route('group', [$event->group->id]) }}">↩
                                Powrót</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered border-dark">
                            <thead>
                            <tr>
                                <th>Nazwa</th>
                                <th>Opis</th>
                                <th>Rozpoczęcie</th>
                                <th>Zakończenie</th>
                                @if($isOwner)
                                    <th>Opcje</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $event->name }}</td>
                                <td>{{ $event->description }}</td>
                                <td>{{ $event->start_time}}</td>
                                <td>{{ $event->end_time}}</td>
                                @if($isOwner)
                                    <td>
                                        <div class="row">
                                            <a href="{{ route('event.edit.form', $event->id) }}"
                                               class="btn btn-outline-primary my-0">Edytuj</a>
                                            <a href="{{ route('event.remove.form', $event->id) }}"
                                               class="btn btn-outline-danger my-0">Usuń</a>
                                        </div>
                                    </td>
                                @endif
                            </tr>
                            </tbody>
                        </table>

                        <br>
                        <form method="POST" action="{{ route('event.finish.post', $event->id) }}">
                            @csrf
                            <p class="w-100 text-center">
                                <button type="submit" class="btn btn-outline-dark">Zakończ wydarzenie</button>
                            </p>
                        </form>
                        <br>
                        <br>

                        <table class="table table-bordered border-dark">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAZWA</th>
                                <th colspan="2">TYP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($event->restaurants as $restaurant)
                                <tr>
                                    <td>{{ $restaurant->id }}</td>
                                    <td>{{ $restaurant->name }}</td>
                                    <td>
                                        @foreach($restaurant->types as $type)
                                            {{ $type->name }}
                                            <br>
                                        @endforeach
                                    </td>
                                    <td class="px-1 text-center">
                                        <button type="button" id="{{$restaurant->id}}"
                                                class="btn btn-sm btn-info handcursor modal-restaurant-button restaurant-dishes-show mx-0"
                                                data-toggle="modal"
                                                value="http://food-senate.test/restaurant/{{$restaurant->id}}/dishes"
                                                data-target=".modal-restaurant">Więcej
                                        </button>

                                        @if($restaurant->isVoted($event->id))
                                            <form method="POST"
                                                  action="{{ route('vote.post',['event_id' => $event->id, 'restaurant_id' => 0]) }}">
                                                @csrf
                                                <button type="submit"
                                                        class="btn btn-sm btn-warning handcursor mx-0">
                                                    Anuluj głos
                                                </button>
                                            </form>
                                        @else
                                            <form method="POST"
                                                  action="{{ route('vote.post',['event_id' => $event->id, 'restaurant_id' => $restaurant->id]) }}">
                                                @csrf
                                                <button type="submit"
                                                        class="btn btn-sm btn-success handcursor mx-0">
                                                    Głosuj
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/dishes.js')  }}"></script>
@endsection
