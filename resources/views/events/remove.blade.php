@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Czy na pewno checesz usunąć grupę:
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('event.remove', $event->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nazwa wydarzenia:</label>

                                <label id="name" class="col-md-4 col-form-label text-md-left">{{ $event->name }}</label>
                            </div>
                            @if($event->description!='')
                                <div class="form-group row">
                                    <label for="description"
                                           class="col-md-4 col-form-label text-md-right">Opis:</label>
                                    <label id="location"
                                           class="col-md-4 col-form-label text-md-left">{{ $event->description }}</label>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="location"
                                       class="col-md-4 col-form-label text-md-right">Start:</label>

                                <label id="location"
                                       class="col-md-4 col-form-label text-md-left">{{ $event->start_time }}</label>
                            </div>
                            <div class="form-group row">
                                <label for="location"
                                       class="col-md-4 col-form-label text-md-right">Koniec:</label>

                                <label id="location"
                                       class="col-md-4 col-form-label text-md-left">{{ $event->end_time }}</label>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-danger">
                                        Usuń
                                    </button>
                                    <a href="{{ route('group', $event->group->id) }}" class="btn btn-primary">Anuluj</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
