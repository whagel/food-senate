@extends('root')
@extends('modals.dishes')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <label><strong>Informacje o wydarzeniu {{ $event->name }}</strong></label>
                        <div style="float:right">
                            <a class="btn btn-success" href="{{ URL::route('group', [$event->group->id]) }}">↩
                                Powrót</a>
                        </div>
                    </div>
                    <div class="card-body">
                        Wydarzenie zakończone.
                        <br>Wybrana restauracja:
                        <table class="table table-bordered border-dark">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAZWA</th>
                                <th colspan="2">TYP</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $winner->id }}</td>
                                <td>{{ $winner->name }}</td>
                                <td>
                                    @foreach($winner->types as $type)
                                        {{ $type->name }}
                                        <br>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ URL::asset('js/dishes.js')  }}"></script>
@endsection
