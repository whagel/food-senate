@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Czy chcesz odbanować użytkownika:
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('user.unban', $user->id) }}">
                            @csrf
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nazwa użytkownika:</label>

                                <label id="name" class="col-md-4 col-form-label text-md-left">{{ $user->name }}</label>

                            </div>
                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Adres email:</label>

                                <label id="name" class="col-md-4 col-form-label text-md-left">{{ $user->email }}</label>

                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-success handcursor">
                                        Odbanuj
                                    </button>

                                    <a href="{{ URL::previous() }}" class="btn btn-outline-primary">Anuluj</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
