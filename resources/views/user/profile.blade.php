@extends('root')

@section('content')
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <form method="POST" action="{{ route('user.edit.avatar.post') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">Zmień avatar</div>
                        <div class="card-body">

                            <div class="card-body">
                                Aktualny avatar:
                                <img class="img-thumbnail avatar-img" src="{{ $user->getAvatarPath() }}"
                                     alt="Ni mo">
                                <br/>
                                <input type="file" name="avatar" id="user-image">

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Zmień
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <form method="POST" action="{{ route('user.edit.profile.post') }}" novalidate>
                        @csrf
                        <div class="card-header">Zmień nazwę</div>
                        <div class="card-body">
                            <div class="form-group row">

                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">Nowa nazwa</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $user->name }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user-description"
                                       class="col-md-4 col-form-label text-md-right">Opis</label>
                                <div class="col-md-6">
                                        <textarea class="form-control" id="user-description" name="description"
                                                  placeholder="Tu zamieść opis swój opis">{{ $user->description }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Zmień
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
