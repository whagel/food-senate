@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('user.edit.password.post') }}">
                            @csrf
                            <div class="card-header">Zmień hasło</div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="current-password"
                                           class="col-md-4 col-form-label text-md-right">Aktualne hasło</label>

                                    <div class="col-md-6">
                                        <input id="current-password" type="password" class="form-control"
                                               name="current-password"
                                               required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password"
                                           class="col-md-4 col-form-label text-md-right">Nowe hasło</label>
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password"
                                               required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm"
                                           class="col-md-4 col-form-label text-md-right">Powtórz hasło</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Zmień
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
