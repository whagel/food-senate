<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css"
          integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/helper.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Comfortaa&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Croissant+One&subset=latin,latin-ext' rel='stylesheet'
          type='text/css'>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"/>
</head>
<body>

<div>
    <nav class="navbar navbar-expand-lg navbar-inverse fixed-top bg-dark py-2">
        <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ URL::asset('logo.png') }}"></a>
        <button class="btn btn-outline-light navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
            <ul class="navbar-nav left-navbar">
                @guest
            </ul>

            <ul class="navbar-nav right-navbar">
                <li class="nav-item my-auto ">
                    <a class="nav-link text-white" href="{{ route('login') }}">Zaloguj się</a>
                </li>
                <li class="nav-item active my-auto text-white">
                    <a class="nav-link text-white" href="{{ route('register') }}">Rejestracja</a>
                </li>
            </ul>
            @else
                <li class="nav-item my-auto">
                    <a class="nav-link text-white" href="{{ route('public.group.list') }}">Publiczne Grupy</a>
                </li>
                <li class="nav-item my-auto">
                    <a class="nav-link text-white" href="{{ route('user.groups') }}">Moje Grupy</a>
                </li>
                <li class="nav-item my-auto">
                    <a class="nav-link text-white" href="{{ route('group.create') }}">Utwórz grupę</a>
                </li>
                </ul>
                <ul class="navbar-nav right-navbar">
                    <li class="nav-item form-inline">
                        <div class="dropdown">
                            <button class="dropbtn bg-transparent my-0 py-0 mx-0 px-0">
                                <a class="nav-link">
                                    Witaj <strong>{{ Auth::user()->name}} </strong><i class="fa fa-caret-down"></i>
                                </a>
                            </button>
                            <div class="dropdown-content bg-dark">
                                <a class="nav-link active py-2 my-0" href="{{ route('user.profile')}}">Mój profil</a>
                                <a class="nav-link active py-2 my-0" href="{{ route('user.edit.profile') }}">Edytuj
                                    profil</a>
                                <a class="nav-link active py-2 my-0" href="{{ route('user.edit.password') }}">Zmień
                                    hasło</a>
                                @if(Auth::user()->isAdmin()||Auth::user()->isSuperAdmin())
                                    <a class="nav-link active py-2 my-0" href="{{ route('admin.panel') }}">Panel
                                        administracyjny</a>
                                @endif
                                <div class="dropdown-divider py-0 my-0"></div>
                                <a class="nav-link active py-2 my-0" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    Wyloguj
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest
        </div>
    </nav>
</div>

<div class="container maincontainer">
    @auth
        <div class="row">

            <div class="container menu_group_button px-0 col-md-3 mb-2">
                <a href="{{ route('public.group.list') }}" class="menu-button">
                    <div class="bg-white text-center w-100 h-100 form-inline">
                        <p class="w-100 py-0 my-0 text-center">PUBLICZNE GRUPY</p>
                    </div>
                </a>
            </div>
            <div class="container menu_group_button px-0 col-md-3 mb-2">
                <a href="{{ route('user.groups') }}" class="menu-button">
                    <div class="bg-white text-center w-100 h-100 form-inline">
                        <p class="w-100 py-0 my-0 text-center">MOJE GRUPY</p>
                    </div>
                </a>
            </div>
            <div class="container menu_group_button px-0 col-md-3 mb-2">
                <a href="{{ route('group.create') }}" class="menu-button">
                    <div class="bg-white text-center w-100 h-100 form-inline">
                        <p class="w-100 py-0 my-0 text-center">UTWÓRZ GRUPĘ</p>
                    </div>
                </a>
            </div>
        </div>


    @endauth

    @include('alerts')
    @yield('content')

</div>
<script src="{{ URL::asset('js/moment.js')  }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript">
    $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
        icons: {
            time: 'far fa-clock',
            date: 'far fa-calendar',
            up: 'fas fa-angle-up',
            down: 'fas fa-angle-down',
            previous: 'fas fa-angle-left',
            next: 'fas fa-angle-right',
            today: 'far fa-calendar-check-o',
            clear: 'far fa-trash',
            close: 'far fa-times'
        }
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js"></script>


@yield('scripts')

</body>
</html>
