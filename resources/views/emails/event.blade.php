<p class="klasa2"
   style="padding: 0;margin: 10px;color: #363e45;border: 2px solid rgb(203,229,234);background-color: rgb(255,255,255)">
    <font size="4">Witaj {{$user->name}}!<br/></font><font size="3">Zbliża się wydarzenie
        <strong>{{ $event->name }}</strong> w grupie <strong>{{$group->name}}</strong></font>

    <font size="3">
        Szczegóły wydarzenia:<br/><br/>

        <strong>Czas start:</strong> {{$event->start_time}}<br/>
        <strong>Koniec:</strong> {{$event->end_time}}<br/>
        <strong>Lokacja:</strong> {{$group->location}}<br/><br/>

        @if($event->description!='')
            Dodatkowe informacje: {{$event->description}}<br/>
        @endif
    </font>
</p>
<p class="klasa2"
   style="padding: 0;margin: 10px;color: #faffcb;border: 3px solid rgb(203,229,234);background-color: rgb(161,197,217)">
    <font size="4">Do zobaczenia!</font><br/>
    <font size="3">Zespół Food-Senate</font></p>
