<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<font size="4">Witaj!<br/></font>
<font size="3"><br/>Oto twój link aktywujący konto na stronie Food-Senate:</font>
<div class="container text-md-center">
    <font size="3.5"><a href="food-senate.test/activation/{{$id}}/{{$token}}" class="btn btn-outline-success">Aktywuj
            konto<br/><br/></a></font>
</div>
<p class="klasa2"
   style="color: #faffcb;border: 3px solid rgb(203,229,234);background-color: rgb(161,197,217)">
    <font size="4">Do zobaczenia!</font><br/>
    <font size="3">Zespół Food-Senate</font></p>
</body>
