@extends('root')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($user->isBanned())
                        <div class="card-header"><strong>Użytkownik zablokowany</strong></div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img class="img-thumbnail avatar-img" src="{{ $user->getAvatarPath() }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                    @else
                        @if($user->id == Auth::user()->id)
                            <div class="card-header"><strong>Mój profil</strong></div>
                        @else
                            <div class="card-header">Profil użytkownika {{ $user->name }}</div>
                        @endif
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img class="img-thumbnail avatar-img"
                                             src="{{ $user->getAvatarPath() }}">
                                    </div>
                                    <div class="col-sm-6">
                                        <strong>Opis: </strong><br/>
                                        {{ $user->description }}
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($user->id == Auth::user()->id)
                        <div class="card-header">Grupy, w których jesteś:</div>
                    @else
                        <div class="card-header">Grupy, w któych jest {{ $user->name }}:</div>
                    @endif
                    <div class="card-body">
                        <table class="table table-bordered border-dark">
                            <tbody>
                            @foreach($groups as $group)
                                <tr>
                                    <td><a href="{{ route('group', $group->id) }}"
                                           class="btn btn-outline-primary border-0">{{ $group->name }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection