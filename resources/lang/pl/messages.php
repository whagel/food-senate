<?php
return [
    'USER' => 'Użytkownik',
    'BANNED' => 'Zbanowany',
    'ADMIN' => 'Administrator',
    'SUPERADMIN' => 'Super administrator',
];