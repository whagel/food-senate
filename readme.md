Instrukcja instalacji 
```
composer install
php artisan migrate
php artisan db:seed
php artisan storage:link
```

Jeśli coś związanego z bazą danych nie działa, prawdopodobnie można to naprawić używając:
```
composer dump-autoload
```